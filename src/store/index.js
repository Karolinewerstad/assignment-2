import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categoryId: "",
        difficulty: "",
        numberOfQuestions: "",
        score: 0,
        questionsWithAnswers: [],
    },
    mutations: {
        setCategoryId: (state, payload) => {
            state.categoryId = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setNumberOfQuestions: (state, payload) => {
            state.numberOfQuestions = payload
        },
        addPoints: (state) => {
            state.score = state.score + 10
        },
        addQuestionWithAnswers: (state, payload) => {
            state.questionsWithAnswers.push(payload)
        },
        resetPoints: (state) => {
            state.score = 0
        },
        resetQuestionsWithAnswers: (state) => {
            state.questionsWithAnswers = []
        }
    },
    actions: {},
    getters: {
        getCategoryId: (state) => {
            return state.categoryId
        },
        getDifficulty: (state) => {
            return state.difficulty
        },
        getNumberOfQuestions: (state) => {
            return state.numberOfQuestions
        },
        getQuestionsWithAnswers: (state) => {
            return state.questionsWithAnswers
        },
        getScore: (state) => {
            return state.score
        }
    }
})