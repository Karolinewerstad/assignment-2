import VueRouter from 'vue-router';
import StartScreen from './views/StartScreen.vue';
import QuestionScreen from './views/QuestionScreen.vue';
import ResultScreen from './views/ResultScreen.vue';

const routes = [
    {
        path: '/',
        name: 'StartScreen',
        component: StartScreen
    },
    {
        path: '/questionScreen',
        name: 'QuestionScreen',
        component: QuestionScreen
    },
    {
        path: '/resultScreen',
        name: 'ResultScreen',
        component: ResultScreen
}];

const router = new VueRouter({ routes });

export default router;